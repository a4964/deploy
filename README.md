# Deploy

Repository con materiale e documentazione di come effettuare il deploy.

## Prerequisiti

- accesso ad internet per scaricare le immagini Docker e alcuni componenti del frontend
- Docker
- docker-compose
- le seguenti porte libere: `8080`, `5000`, `5001`, `3316`, `3326`, `5672`, `15672`

## Deploy dell'infrastruttura

Per poter avere tutta l'infrastruttura up&running, posizionarsi nella cartella dove è presente il docker-compose.yml e lanciare il seguente comando:

```
docker-compose up -d

```

## Come testare il flusso?

Per testare il flusso completo, ci si collega al frontend attraverso questo indirizzo:

http://localhost:8080/

Premere il bottone "Invia" e sarà possibile vedere dal log di docker dei messaggi di questo tipo:

```
service-api | info: ServiceApi.Controllers.MessageController[0]
service-api | Getting data from ServiceFrontend: [[{"id":1,"value":10},{"id":2,"value":30}]]
service-api | info: ServiceApi.Service.BusService[0]
service-api | Saved message [1]

service-satellite | Getting data from ServiceApi: IdMessage [1] Value [40]
service-satellite | info: ServiceSatellite.Services.ReferenceService[0]
service-satellite | Saved reference [1]

service-api | Getting data from ServiceSatellite: IdMessage [1] IdReference [1] ReferenceDateTime [10/23/2021 13:58:16]
service-api | info: ServiceApi.Service.BusService[0]
service-api | Updated message [1]
```

## Accedere ai database

Per poter accedere ai database (su localhost), per controllare o modificare i dati le informazioni di accesso sono le seguenti.

### ServiceApi database

- Username: `service-api-user`
- Password: `password`
- Porta: `3316`

### ServiceSatellite database

- Username: `service-satellite-user`
- Password: `password`
- Porta: `3326`

### Accesso root database

L'accesso con utente root per entrambi i database è il seguente:

- Username: `root`
- Password: `admin`

## Swagger API

La documentazione delle ServiceApi è visualizzabile al seguente link:

http://localhost:5000/swagger

## RabbitMQ - Management

Il servizio di RabbitMQ installa anche la console di management che è consultabile, una volta avviato il container, a questo indirizzo:

http://localhost:15672

Le credenziali (di default) sono le seguenti:

- Username: `guest`
- Password: `guest`
